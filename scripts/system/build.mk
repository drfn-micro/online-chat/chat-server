build: ## Build project binaries
build: build-app

build-debug:	## Build application for debug
build-debug:
	CGO_ENABLE=0 go build ${DEBUG_GCFLAGS} -o bin/${APP_BINARY} cmd/${APP_NAME}/*.go

build-app: ## Build application
build-app:
	go build -trimpath ${LDFLAGS} -o bin/${APP_BINARY} cmd/${APP_NAME}/*.go

.PHONY: build build-app build-debug
