all: run

run:		## Run application
run:
	go run ./cmd/${APP_NAME}

run-debug: 	## Run application with remote debug
	dlv --listen=:2345 --headless=true --api-version=2 exec bin/${APP_BINARY}

none:		## Wait to run application
	sleep 31536000

.PHONY: all run run-debug none
