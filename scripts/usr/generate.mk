wire:
	@wire ./...

proto:
	@mkdir -p pkg/${APP_DIR}
	@find api/ \
		-iname "*.proto" \
		-exec protoc \
			--proto_path . --go_out=pkg/${APP_DIR} \
			--go_opt=paths=source_relative --go-grpc_out=pkg/${APP_DIR} \
			--go-grpc_opt=paths=source_relative {} \;

	@find internal/ \
		-iname "*.proto" \
		-exec protoc \
			--proto_path=. \
			--go_out=paths=source_relative:. \
			--go-grpc_out=paths=source_relative:. {} \;
.PHONY: wire proto
